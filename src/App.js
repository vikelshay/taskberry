import './App.css';
import {AppBar, Container, IconButton, Toolbar, Typography} from "@material-ui/core";
import React from "react";
import Layout from "./components/Layout";

const App = () => {

    return (
        <Container maxWidth={false}>
            <AppBar position="static">
                <Toolbar>
                    <IconButton edge="start" color="inherit" aria-label="menu">
                    </IconButton>
                    <Typography variant="h6">
                        TasksBerry
                    </Typography>
                </Toolbar>
            </AppBar>
            <Layout/>
        </Container>
    );
}

export default App;
