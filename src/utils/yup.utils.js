import * as yup from "yup";

const minLength = 3;
const maxLength = 15;
const minDays = 1;
const maxDays = 100;

export const validationSchema = yup.object({
    headline: yup
        .string('Enter Task Name')
        .min(minLength, `Headline must have at least ${minLength} characters`)
        .max(maxLength, `Headline must have at most ${maxLength} characters`)
        .required('Headline is required'),
    content: yup
        .string('Enter Content')
        .required('Content is required'),
    maxDays: yup
        .number('Enter Days Number')
        .min(minDays, `Minimum Days is ${minDays}`)
        .max(maxDays, `Maximum Days is ${maxDays}`)
        .required('Maximum Days is required'),
});
