export function descendingComparator(a, b, orderBy) {
    if(orderBy === 'deadline'){
        return deadLineComparator(b, a);
    }
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

export function getComparator(order, orderBy) {
    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
}

export function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}

export function getDeadLine(creationTime, maxDays) {
    return new Date(creationTime + 1000 * 60 * 60 * 24 * maxDays).toLocaleDateString()
}

function deadLineComparator(b, a) {
    if (b['creationTime'] + 1000 * 60 * 60 * 24 * b['maxDays'] < a['creationTime'] + 1000 * 60 * 60 * 24 * a['maxDays']) {
        return -1;
    }
    if (b['creationTime'] + 1000 * 60 * 60 * 24 * b['maxDays'] > a['creationTime'] + 1000 * 60 * 60 * 24 * a['maxDays']) {
        return 1;
    }
    return 0;
}
