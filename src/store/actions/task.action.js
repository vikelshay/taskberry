import * as actionTypes from './actionTypes';

export const createTask = (task) =>{
    return {
        type: actionTypes.CREATE_TASK,
        payload: {task}
    };
};

export const deleteTasks = (selectedTasks) =>{
    return {
        type: actionTypes.DELETE_TASKS,
        payload: {selectedTasks}
    };
};

export const updateFilterString = (filterString) =>{
    return {
        type: actionTypes.UPDATE_FILTER_STRING,
        payload: {filterString}
    };
};

