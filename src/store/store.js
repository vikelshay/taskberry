import {combineReducers, createStore} from "redux";
import taskReducer from "./reducers/task.reducer";
import {loadState, saveState} from "../utils/localStorage";
import {composeWithDevTools} from "redux-devtools-extension";

const rootReducer = combineReducers({
    task: taskReducer
});

const persistedState = loadState();

export const store = createStore(rootReducer, persistedState, composeWithDevTools() );

store.subscribe(() => {
    saveState({
        state: store.getState()
    });
});

