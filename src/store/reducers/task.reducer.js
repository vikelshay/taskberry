import * as actionTypes from '../actions/actionTypes';

const initialState = {
    tasks: [],
    idSequence: 1,
    filterString: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type){
        case actionTypes.CREATE_TASK:
            const newTask = {
                ...action.payload.task,
                id: state.idSequence
            }
            return {
                ...state,
                tasks: state.tasks.concat(newTask),
                idSequence: state.idSequence + 1
            };
        case actionTypes.DELETE_TASKS:
            const newTasks = state.tasks.filter(({id}) => !action.payload.selectedTasks.includes(id));

            return {
                ...state,
                tasks: newTasks
            };
        case actionTypes.UPDATE_FILTER_STRING:
            const filterString = action.payload.filterString;

            return {
                ...state,
                filterString: filterString
            };
        default:
            return state;
    }

}

export default reducer;
