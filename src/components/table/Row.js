import React from "react";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import {Box, Collapse} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import CheckBoxRow from "./CheckboxRow";

const Row = (props) => {
    const {row, index, selected, setSelected} = props;
    const [open, setOpen] = React.useState(false);

    return (
        <React.Fragment key={row.id}>
            <CheckBoxRow row={row} index={index} selected={selected} setSelected={setSelected} open={open} setOpen={setOpen}/>
            <TableRow>
                <TableCell style={{paddingBottom: 0, paddingTop: 0}} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box margin={1}>
                            <Typography variant="h6" gutterBottom component="div">
                                Content
                            </Typography>
                            {row.content}
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}

export default Row;
