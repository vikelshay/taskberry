import EnhancedTable from "./table/TasksTable";
import React from "react";
import FormDialog from "./dialog/FormDialog";
import {connect} from "react-redux";

const Layout = (props) => {
    const placeHolder = props.tasks.length > 0 ? <EnhancedTable/> : null;

    return (
        <React.Fragment>
            <FormDialog/>
            {placeHolder}
        </React.Fragment>
    );
}

const mapStateToProps = state => {
    return {
        tasks: state.task.tasks,
    };
};

export default connect(mapStateToProps)(Layout);
