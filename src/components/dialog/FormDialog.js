import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useFormik } from 'formik';
import {connect} from "react-redux";
import {createTask} from "../../store/actions";
import {validationSchema} from "../../utils/yup.utils";

const FormDialog = (props) => {
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    }

    const handleTaskCreation = () => {
        let task = {
            headline: formik.values.headline,
            content: formik.values.content,
            maxDays: formik.values.maxDays,
            creationTime: Date.now()
        }
        if(formik.isValid){
            setOpen(false);
            props.onTaskCreation(task);
        }
    };

    const formik = useFormik({
        initialValues: {
            headline: 'Task Headline',
            content: 'Create TasksBerry App',
            maxDays: 1
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            alert('Task was added: ' + values.headline);
        }
    });

    return (
        <React.Fragment>
            <Button variant="outlined" color="primary" onClick={handleClickOpen} style={{margin: '1% 0 1% 0'}}>
                Create a task
            </Button>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Add a new task</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Please fill the following details
                    </DialogContentText>
                    <form onSubmit={formik.handleSubmit}>
                        <TextField
                            fullWidth
                            id="headline"
                            name="headline"
                            label="headline"
                            value={formik.values.headline}
                            onChange={formik.handleChange}
                            error={formik.touched.headline && Boolean(formik.errors.headline)}
                            helperText={formik.touched.headline && formik.errors.headline}
                        />
                        <TextField
                            fullWidth
                            id="content"
                            name="content"
                            label="content"
                            value={formik.values.content}
                            onChange={formik.handleChange}
                            error={formik.touched.content && Boolean(formik.errors.content)}
                            helperText={formik.touched.content && formik.errors.content}
                        />
                        <TextField
                            fullWidth
                            id="maxDays"
                            name="maxDays"
                            label="maxDays"
                            type="number"
                            value={formik.values.maxDays}
                            onChange={formik.handleChange}
                            error={formik.touched.maxDays && Boolean(formik.errors.maxDays)}
                            helperText={formik.touched.maxDays && formik.errors.maxDays}
                        />
                        <DialogActions>
                            <Button onClick={handleClose} color="primary">
                                Cancel
                            </Button>
                            <Button onClick={handleTaskCreation} color="primary" type="submit">
                                Create Task
                            </Button>
                        </DialogActions>
                    </form>
                </DialogContent>

            </Dialog>
        </React.Fragment>
    );
}

const mapDispatchToProps = dispatch => {
    return {
        onTaskCreation: (task) => dispatch(createTask(task)),
    }
};

export default connect(null ,mapDispatchToProps)(FormDialog);
